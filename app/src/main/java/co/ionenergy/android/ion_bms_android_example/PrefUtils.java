package co.ionenergy.android.ion_bms_android_example;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtils {

    private static final String PREF_EDISON_USERNAME = "edison_username";
    private static final String PREF_EDISON_PASSWORD = "edison_password";

    private static SharedPreferences getPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setEdisonUsername(Context context, String username) {
        getPref(context).edit().putString(PREF_EDISON_USERNAME, username).apply();
    }

    public static String getEdisonUsername(Context context) {
        return getPref(context).getString(PREF_EDISON_USERNAME, "");
    }

    public static void setEdisonPassword(Context context, String pass) {
        getPref(context).edit().putString(PREF_EDISON_PASSWORD, pass).apply();
    }

    public static String getEdisonPassword(Context context) {
        return getPref(context).getString(PREF_EDISON_PASSWORD, "");
    }

}

