package co.ionenergy.android.ion_bms_android_example;

import android.bluetooth.BluetoothDevice;

public class CustomBluetoothDevice {

    private BluetoothDevice device;

    public CustomBluetoothDevice(BluetoothDevice device) {
        this.device = device;
    }

    public String getDeviceName() {
        if(device.getName() != null && !device.getName().isEmpty()) {
            return device.getName();
        } else {
            return device.getAddress();
        }
    }

    public String getDeviceMacAddress() {
        return device.getAddress();
    }

    @Override
    public String toString() {
        if (device != null) {
            if(device.getName() != null && !device.getName().isEmpty()) {
                return device.getName();
            } else {
                return device.getAddress();
            }
        }
        // fallback name
        return "";
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof CustomBluetoothDevice){
            CustomBluetoothDevice ptr = (CustomBluetoothDevice) v;
            if(ptr.device != null) {
                retVal = ptr.device.getAddress().equals(this.device.getAddress());
            }
        }
        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.getDeviceMacAddress() != null ? this.getDeviceMacAddress().hashCode() : 0);
        return hash;
    }
}
