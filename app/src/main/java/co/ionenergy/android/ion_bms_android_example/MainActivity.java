package co.ionenergy.android.ion_bms_android_example;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import co.ionenergy.android.ionbmsmanager.Battery;
import co.ionenergy.android.ionbmsmanager.IonBluetoothLeManager;
import co.ionenergy.android.ionbmsmanager.data.model.BmsRecord;
import co.ionenergy.android.ionbmsmanager.data.model.TemperatureSensor;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getSimpleName();

    private Button bt_browse;
    private Button bt_connect;
    private Button bt_disconnect;
    private TextView tv_connectionStatus;
    private EditText et_accessPointName;
    private LinearLayout ll_connectForm;
    private LinearLayout ll_bmsData;
    private TextView tv_soc;
    private TextView tv_current;
    private TextView tv_totalVoltage;
    private TextView tv_temperature;
    private ArrayAdapter<CustomBluetoothDevice> arrayAdapterBluetooth;
    private List<CustomBluetoothDevice> discoveredBluetoothDevices = new ArrayList<>();
    private CustomBluetoothDevice targetedBluetoothDevice;

    public static void launch(Activity callingActivity) {
        Intent intent = new Intent(callingActivity, MainActivity.class);
        callingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_browse = (Button) findViewById(R.id.bt_browse);
        bt_connect = (Button) findViewById(R.id.bt_connect);
        bt_disconnect = (Button) findViewById(R.id.bt_disconnect);
        tv_connectionStatus = (TextView) findViewById(R.id.tv_connectionStatus);
        et_accessPointName = (EditText) findViewById(R.id.et_accessPointName);
        ll_connectForm = (LinearLayout) findViewById(R.id.ll_connectForm);
        ll_bmsData = (LinearLayout) findViewById(R.id.ll_bmsData);
        tv_soc = (TextView) findViewById(R.id.tv_soc);
        tv_current = (TextView) findViewById(R.id.tv_current);
        tv_totalVoltage = (TextView) findViewById(R.id.tv_totalVoltage);
        tv_temperature = (TextView) findViewById(R.id.tv_temperature);

        //Initialize ION Bluetooth Low Energy manager by providing user connected Edison credentials.
        IonBluetoothLeManager.getInstance().init(this, PrefUtils.getEdisonUsername(this), PrefUtils.getEdisonPassword(this));

        //Add listeners to all views.
        addListeners();

        arrayAdapterBluetooth = new ArrayAdapter<CustomBluetoothDevice>(MainActivity.this, android.R.layout.select_dialog_singlechoice,
            discoveredBluetoothDevices);

    }

    @Override
    protected void onResume() {
        //Start BLE service : Make sure to disable any feature related to ble until service has started.
        IonBluetoothLeManager.getInstance().startService();
        //Register the local broadcast receiver for Ion BLE Manager
        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(ionBleBroadcastReceiver, new IntentFilter(IonBluetoothLeManager.BROADCAST_RECEIVER_ION_BLE_FILTER));

        //Check if android permissions are granted to use BLE.
        checkPermission();


        //Register receiver to discover nearby Bluetooth access points
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(bluetoothDiscoveryReceiver, filter);
            adapter.startDiscovery();
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        //Stop BLE service : Disable feature related to BLE when service has stopped
        IonBluetoothLeManager.getInstance().stopService();
        //Unregister the local broadcast receiver for Ion BLE Manager
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(ionBleBroadcastReceiver);

        //Cancel Bluetooth discovery
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null) {
            adapter.cancelDiscovery();
            unregisterReceiver(bluetoothDiscoveryReceiver);
        }

        super.onPause();
    }

    private void checkPermission() {
        if(!PermissionUtils.areBleAppPermissionsGranted(this)) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[] { Manifest.permission.ACCESS_FINE_LOCATION },1);
        }
    }

    private void addListeners() {
        bt_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, "No bluetooth module on this device.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!mBluetoothAdapter.isEnabled()) {
                    Toast.makeText(MainActivity.this, "Please enable bluetooth module.", Toast.LENGTH_SHORT).show();
                    return;
                }

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
                builderSingle.setTitle(R.string.select_device);

                builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapterBluetooth,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            targetedBluetoothDevice = (arrayAdapterBluetooth.getItem(which));
                            et_accessPointName.setText(targetedBluetoothDevice.getDeviceName());
                        }
                    });

                builderSingle.show();
            }
        });

        bt_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(targetedBluetoothDevice != null) {
                    //Connect to the targeted device : you have to use the mac address as parameter to connect to the BMS.
                    IonBluetoothLeManager.getInstance().connect(targetedBluetoothDevice.getDeviceMacAddress());
                } else {
                    Toast.makeText(MainActivity.this, "Please select a Bleutooth device.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        bt_disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IonBluetoothLeManager.getInstance().disconnect();
            }
        });
    }

    private final BroadcastReceiver bluetoothDiscoveryReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //discovery starts, we can show progress dialog or perform other tasks
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //discovery finishes, dismis progress dialog
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //bluetooth device found
                BluetoothDevice _device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                CustomBluetoothDevice device = new CustomBluetoothDevice(_device);

                if (!discoveredBluetoothDevices.contains(device)) {
                    discoveredBluetoothDevices.add(device);
                }
            }
        }
    };


    private BroadcastReceiver ionBleBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_SERVICE_STARTED)) {
                    //BLE service has started. We can enable feature related to BLE.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_SERVICE_STARTED));
                    setBmsConnected(false);
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_SERVICE_STOPPED)) {
                    //BLE service has stopped. We must disable feature related to BLE.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_SERVICE_STOPPED));
                    setBmsConnected(false);
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_SCANNING)) {
                    //Connection step 1 : we start scanning to make sure targeted device is available.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_SCANNING));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_DEVICE_FOUND)) {
                    //Connection step 2 : device has been found.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_DEVICE_FOUND));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_CONNECTING)) {
                    //Connection step 3 : start BLE connecting process.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_CONNECTING));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_CONNECTED)) {
                    //Connection step 4 : android device connected with the BMS.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_CONNECTED));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_ESTABLISHING_COMMUNICATION)) {
                    //Connection step 5 : android device try to communicate with BMS by requesting battery data.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_ESTABLISHING_COMMUNICATION));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_COMMUNICATING)) {
                    //Connection step 6 : communication established with the BMS! It will send battery data periodically.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_COMMUNICATING));
                    setBmsConnected(true);
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_CONNECTION_FAILED)) {
                    //Connection failed with the targeted BMS.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_DISCONNECTED));
                    setBmsConnected(false);
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_BATTERY_UPDATED)) {
                    //Receive lastest battery state
                    updateBatteryDisplay(IonBluetoothLeManager.getInstance().getConnectedBattery());
                    Log.d(TAG, "Battery updated!");
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_DISCONNECTED)) {
                    //BMS disconnected from android device.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_DISCONNECTED));
                    setBmsConnected(false);
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_DEVICE_NOT_FOUND)) {
                    //The BMS access point name has not been found during scanning process.
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_DEVICE_NOT_FOUND));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_NOT_ENABLED)) {
                    //Android phone ble module is off
                    tv_connectionStatus.setText(intent.getStringExtra(IonBluetoothLeManager.EXTRA_BLE_NOT_ENABLED));
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BMS_HISTORICAL_DATA)) {
                    ArrayList<BmsRecord> records = extras.getParcelableArrayList(IonBluetoothLeManager.EXTRA_BMS_HISTORICAL_DATA);
                    Log.d(TAG, "Battery historical data received!");
                }

                /*Other events fired by the BLE manager not used in this application example.
                  Please check documentation for more information*/

                /*else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_CHUNK_RECEIVED)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_OPERATION_PROGRESS)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_GET_CONF_SUCCESS)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_GET_CONF_FAILED)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_SET_CONF_TERMINATED)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_BATTERY_GET_SD_CARD_SIZE)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_BATTERY_GET_SD_CARD_TERMINATED)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_BMS_AUTH_SUCCESS)) {
                } else if (extras.containsKey(IonBluetoothLeManager.EXTRA_BLE_BMS_AUTH_FAILED)) {
                }*/
            }
        }
    };


    //Change activity display when BMS is connected or not.
    private void setBmsConnected(boolean connected) {
        ll_connectForm.setVisibility(connected ? View.GONE : View.VISIBLE);
        ll_bmsData.setVisibility(connected ? View.VISIBLE : View.GONE);
        bt_disconnect.setVisibility(connected ? View.VISIBLE : View.GONE);
    }

    //Update battery data
    private void updateBatteryDisplay(Battery battery) {
        tv_soc.setText(String.valueOf(battery.getSoc()));
        tv_current.setText(String.valueOf(battery.getCurrent()));
        tv_totalVoltage.setText(String.valueOf(battery.getTotalVoltage()/1000.0));
        tv_temperature.setText(String.valueOf(battery.getTemperatureSensors().get(0).getTemperature(TemperatureSensor.TEMPERATURE_CELSIUS)));
    }

}
